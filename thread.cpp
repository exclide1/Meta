#define _GLIBCXX_DEBUG 1
#include <iostream>
#include <list>
#include <vector>
#include <algorithm>
#include <future>
#include <thread>
using namespace std;


template<class It, class F1, class F2>
auto map_reduce(It p, It q, F1 f1, F2 f2, size_t sz) -> decltype(f2(f1(*p), f1(*p)))
{
	using tp1 = decltype(f1(*p));
    using tp2 = decltype(f2(f1(*p), f1(*p)));
	size_t csz = std::distance(p, q);
	size_t dv = csz / sz;
	vector<vector<It>> iters;
	for (int i = 0; i < sz-1; i++)
	{
		auto st = p;
		advance(p, dv);
		auto nd = p;
		iters.push_back({st, nd});
	}
	iters.push_back({p, q});
	
	vector<tp1> resl(sz);
	vector<std::thread> vt(sz);
	for (int i = 0; i < sz; i++)
	{
		auto st = iters[i][0];
		auto nd = iters[i][1];
		tp1* ress = &resl[i];
		std::thread t([ress, st, nd, &f1, &f2]() { auto stt = st; auto ndd = nd; tp1 res = f1(*stt); 
					while(++stt != ndd) { res = f2(res, f1(*stt)); } *ress = res; });
		vt[i].swap(t);
		
	}
	for (int i = 0; i < sz; i++)
		vt[i].join();
	int i = 0;
	tp2 fres = resl[i];
	while (++i != sz)
		fres = f2(fres, resl[i]);

	return fres;
}


int main()
{
	/*
std::list<int> l = {1,2,3,4,5,6,7,8,9,10,11};
for(int i = 1; i <= l.size(); i++)
{
	cout << "threads: " << i << endl;
	auto sum = map_reduce(l.begin(), l.end(), [](int i){return i;}, std::plus<int>(), i);
	cout << sum << endl;
}
*/

for (int i = 1; i <= 11; i++)
{
	cout << "i: " << i << endl;

const std::vector<std::string> r = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"};
   auto summ = map_reduce(r.begin(), r.end(), [](std::string x){return x;}, std::plus<std::string>(), i);
   cout << "summ: " << summ << endl;
}

std::vector< std::string > const v2 = { "multithread", "and", "async", "in", "C++", "is", "total", "sh!t" };
// If "i <= v2.size()": on Stepik OK, there NOT OK
for ( std::size_t i = 1; i <= 8; ++i )

{
	auto size_sum = map_reduce(v2.begin(), v2.end(), []( std::string s ){ return s.size(); }, 
	std::plus< std::size_t >(), i);
}

	return 0;
}