#define _GLIBCXX_DEBUG 1
#include <iostream>
#include <vector>
#include <tuple>
using namespace std;

template<int... Ints>
struct IntList;

template<int H, int... Ints>
struct IntList<H, Ints...>
{
	static const int Head = H;
	using Tail = IntList<Ints...>;
};
template<>
struct IntList<> {};

template<int x, typename IL>
struct IntCons;

template<int x, int... Ints>
struct IntCons<x, IntList<Ints...>>
{
	using type = IntList<x, Ints...>;
};

template<int n, int i = 0>
struct Generate
{
	using type = typename IntCons<i, typename Generate<n-1, i+1>::type>::type;
};

template<int i>
struct Generate<0, i>
{
	using type = IntList<>;
};

template<typename T>
void check()
{
	cout << __PRETTY_FUNCTION__ << endl;
}

template<typename F, typename T, int... indx>
auto apply(F fnc, T tp, IntList<indx...>) -> decltype(fnc(get<indx>(tp) ...))
{
	return fnc(get<indx>(tp) ...);
}

template<typename F, typename... types>
auto apply(F fnc, tuple<types...> tp) -> decltype(apply(fnc, tp, typename Generate<sizeof...(types)>::type{}))
{
	return apply(fnc, tp, typename Generate<sizeof...(types)>::type{});
}

template<int a, int b>
struct Plus
{
    static int const value = a + b;    
};

template<int a, int b>
struct Minus
{
    static int const value = a - b;    
};


template<typename L1, typename L2, template<int...> class F>
struct Zip;

template<int... int1, int... int2, template<int...> class F>
struct Zip<IntList<int1...>, IntList<int2...>, F>
{
	using type = IntList<F<int1, int2>::value...>;
};

template<class IL>
struct Quantity
{
	Quantity() : val(0) {}
	explicit Quantity(double d) : val(d) {}
	double value() const { return val; }
	double val;
};
	template<typename List>
	Quantity<List> operator+(Quantity<List> x, Quantity<List> y) {	
			return Quantity<List>(x.val + y.val); }
	template<typename List>
	Quantity<List> operator-(Quantity<List> x, Quantity<List> y) {
			return Quantity<List>(x.val - y.val); }
	template<typename L1, typename L2, typename L3 = typename Zip<L1, L2, Plus>::type>
	Quantity<L3> operator*(Quantity<L1> x, Quantity<L2> y)
	{
		return Quantity<L3>(x.val * y.val);
	}
	template<typename L1, typename L2, typename L3 = typename Zip<L1, L2, Minus>::type>
	Quantity<L3> operator/(Quantity<L1> x, Quantity<L2> y)
	{
		return Quantity<L3>(x.val / y.val);
	}
	template<typename L1>
	Quantity<L1> operator*(double d, Quantity<L1> x)
	{
		return Quantity<L1>(x.val * d);
	}
	template<typename L1>
	Quantity<L1> operator*(Quantity<L1> x, double d)
	{
		return Quantity<L1>(x.val * d);
	}
	template<typename L1>
	Quantity<typename Zip<IntList<0,0,0,0,0,0,0>, L1, Minus>::type> operator/(double d, Quantity<L1> x)
	{
		return Quantity<typename Zip<IntList<0,0,0,0,0,0,0>, L1, Minus>::type>(d / x.val);
	}
	template<typename L1>
	Quantity<L1> operator/(Quantity<L1> x, double d)
	{
		return Quantity<L1>(x.val / d);
	}
	
struct Struct 
{ 
    size_t size = 0;  
};

/*   //shit won't compile, but works (SFINAE)
template<class T, size_t (T::*)() const = &T::size> 
size_t get_size(T t)
{
    return t.size();
}
template<class T, size_t T::* = &T::size>
size_t get_size(T t)
{
    return t.size;
}
*/

int main()
{
std::string s{"Hello"};
//cout << get_size(s) << endl; // 5, вызывается метод size()
Struct x{10};
cout << get_size(x) << endl;  // 10, читается поле size

	

	return 0;
}